﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdiBase
{
    public class Ordrsp
    {
        public Ordrsp()
        {
            this.Una = new UNA();
            this.Unb = new UNB();
            this.Unh = new UNH();
            this.Bgm = new BGM();
            this.Dtm = new DTM();
            this.Imd = new IMD();
            this.Rff = new RFF();
            this.Nad = new NAD();
            this.Moa = new MOA();
            this.Pcd = new PCD();
            this.Qty = new QTY();
            this.Lin = new LIN();
            this.Pia = new PIA();
            this.Pri = new PRI();
            this.Cta = new CTA();
            this.Uns = new UNS();
            this.Cnt = new CNT();
            this.Unt = new UNT();
            this.Unz = new UNZ();
        }

        public UNA Una { get; set; }
        public UNB Unb { get; set; }
        public UNH Unh { get; set; }
        public BGM Bgm { get; set; }
        public DTM Dtm { get; set; }
        public IMD Imd { get; set; }
        public RFF Rff { get; set; }
        public NAD Nad { get; set; }
        public MOA Moa { get; set; }
        public PCD Pcd { get; set; }
        public QTY Qty { get; set; }
        public LIN Lin { get; set; }
        public PIA Pia { get; set; }
        public PRI Pri { get; set; }
        public CTA Cta { get; set; }
        public UNS Uns { get; set; }
        public CNT Cnt { get; set; }
        public UNT Unt { get; set; }
        public UNZ Unz { get; set; }
    }


    public class UNA
    {
        public const string Id = "UNA";
        public string CompSeparator { get; set; }
        public string DataSeparator { get; set; }
        public string DecimalNotation { get; set; }
        public string ReleaseIndicatior { get; set; }
        public string SegmentTerminator { get; set; }
    }

    public class UNB
    {
        public const string Id = "UNB";
        public string SyntaxId { get; set; }
        public string SyntaxVersion { get; set; }
        public string SenderId { get; set; }
        public string SenderIdQualifier { get; set; }
        public string RecipientId { get; set; }
        public string RecipientIdQualifier { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string RecipientRef { get; set; }

    }

    public class UNH
    {
        public const string Id = "UNH";
        public string MessageRefNo { get; set; }
        public string MessageTypeId { get; set; }
        public string MessageVersionNo { get; set; }
        public string MessageReleaseNo { get; set; }
        public string ControllingAgency { get; set; }
    }

    public class BGM
    {
        public const string Id = "BGM";
        public string DocumentNameCode { get; set; }
        public string DocumentNumber { get; set; }
        public string MessageFunction { get; set; }
    }

    public class DTM
    {
        public const string Id = "DTM";
        public string DateTimeQualifier { get; set; }
        public string DateTime { get; set; }
        public string DateTimeFormat { get; set; }
    }

    public class IMD
    {
        public const string Id = "IMD";
        public string ItemDescriptionTypecode { get; set; }
        public string ItemCharacteristicCode { get; set; }
        public string ItemDescriptionId { get; set; }
        public string QualifierCode { get; set; }
        public string AgencyCode { get; set; }
        public string ItemDescription { get; set; }
    }

    public class RFF
    {
        public const string Id = "RFF";
        public string ReferenceQualifier { get; set; }
        public string ReferenceNumber { get; set; }
    }

    public class NAD
    {
        public const string Id = "NAD";
        public string PartyQualifier { get; set; }
        public string PartyId { get; set; }
        public string CodeListQualifier { get; set; }
        public string CodeListAgency { get; set; }
        public string NameAndAddress { get; set; }
        public string PartyName { get; set; }
        public string StreetAndNumber { get; set; }
        public string City { get; set; }
        public string CountrySubId { get; set; }
        public string Postcode { get; set; }
        public string CountryCode { get; set; }
    }

    public class MOA
    {
        public const string Id = "MOA";
        public string MonetaryAmountQualifier { get; set; }
        public string MonetaryAmount { get; set; }
    }

    public class PCD
    {
        public const string Id = "PCD";
        public string PercentageQualifier { get; set; }
        public string Percentage { get; set; }
    }

    public class QTY
    {
        public const string Id = "QTY";
        public string QuantityQualifier { get; set; }
        public string Quantity { get; set; }
        public string MeasureUnit { get; set; }
    }

    public class LIN
    {
        public const string Id = "LIN";
        public string LineNumber { get; set; }
    }

    public class PIA
    {
        public const string Id = "PIA";
        public string ProductIdQualifier { get; set; }
        public string ItemNumber { get; set; }
        public string ItemNumberTypecode { get; set; }
    }

    public class PRI
    {
        public const string Id = "PRI";
        public string PriceQualifier { get; set; }
        public string Price { get; set; }
    }

    public class CTA
    {
        public const string Id = "CTA";
        public string ContactCode { get; set; }
        public string ContactId { get; set; }
        public string ContactName { get; set; }
    }

    public class UNS
    {
        public const string Id = "UNS";
        public string SectionId { get; set; }
    }

    public class CNT
    {
        public const string Id = "CNT";
        public string ControlQualifier { get; set; }
        public string ControlValue { get; set; }
    }

    public class UNT
    {
        public const string Id = "UNT";
        public string SegmentsCount { get; set; }
        public string MessageRefNo { get; set; }
    }

    public class UNZ
    {
        public const string Id = "UNZ";
        public string ControlCount { get; set; }
        public string ControlRef { get; set; }
    }
}
